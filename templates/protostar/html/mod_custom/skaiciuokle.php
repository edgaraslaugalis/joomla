<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>


<div class="custom<?php echo $moduleclass_sfx; ?>" <?php if ($params->get('backgroundimage')) : ?> style="background-image:url(<?php echo $params->get('backgroundimage'); ?>)"<?php endif; ?> >
    <div class="jumbotron">
        <div class="container text-center">
            <h1><?php echo $module->title; ?></h1>
    <?php echo $module->content; ?>
    <?php
    $message ='';
    $x = date('Y');
    if(isset($_POST['submit'])){
        $x = date('Y');
        if(!empty($_POST['year'])) {
            $x = (int)$_POST['year'];
        }
        $y=0;
        $galiojimas = array(
            array(1946, 0),
            array(1973, 15),
            array(1994, 25),
            array(1999, 50),
            array(9999, 70),
        );
        $return = 'Nebegalioja';
        foreach ($galiojimas as $galioja){
            if($x < $galioja[0] && $x+$galioja[1] < $galioja[0]){
                if($x + $galioja[1] > date('Y')){
                    $return = 'Nebegalios: ' . ($x + $galioja[1]);
                }
                break;
            }
        }
        $message = $return;
    }
    ?>

    <form action="" method="post">
        <input type="number" min="0001" max="9999" step="1" name="year" value="<?= $x; ?>" /><br/>
        <?php echo $message; ?><br/><br/>
        <input type="submit" name="submit" class="btn btn-primary" value="Tikrinti"/>
    </form>
        </div>
    </div>
</div>
