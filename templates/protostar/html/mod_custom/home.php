<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>


<div class="custom<?php echo $moduleclass_sfx; ?>" <?php if ($params->get('backgroundimage')) : ?> style="background-image:url(<?php echo $params->get('backgroundimage'); ?>)"<?php endif; ?> >
    <!-- Jumbotron -->
    <div class="jumbotron">
        <div class="container text-center">
            <h1><?php echo $module->title; ?></h1>
    <?php echo $module->content; ?>
    <p><a class="btn btn-primary" href="#" role="button">Įgyvendinti projektai</a></p>
</div>
</div>

<!-- Services -->
<div class="services-line pt-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg text-center">
                <div class="border-right d-block pr-3">
                    <span class="icon-lg icon-wireframes icon-muted"></span>
                    <p class="lead text-muted mb-0">Projektavimas ir dizainas</p>
                </div>
            </div>
            <div class="col-12 col-lg text-center">
                <div class="border-right d-block pr-3">
                    <span class="icon-lg icon-testing icon-muted"></span>
                    <p class="lead text-muted">Naudotojų testavimas</p>
                </div>
            </div>
            <div class="col-12 col-lg text-center">
                <div class="border-right d-block pr-3">
                    <span class="icon-lg icon-learning icon-muted"></span>
                    <p class="lead text-muted">Mokymai</p>
                </div>
            </div>
            <div class="col-12 col-lg text-center">
                <div class="border-right d-block pr-3">
                    <span class="icon-lg icon-settings icon-muted"></span>
                    <p class="lead text-muted">Įrankiai</p>
                </div>
            </div>
            <div class="col-12 col-lg text-center">
                <div class="d-block pr-3">
                    <span class="icon-lg icon-icon-education-green icon-muted"></span>
                    <p class="lead text-muted">Skaitmeninimas ir duomenų tvarkymas</p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
