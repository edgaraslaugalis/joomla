<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/** @var JDocumentHtml $this */

$app  = JFactory::getApplication();
$user = JFactory::getUser();

// Output as HTML5
$this->setHtml5(true);

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');
$siteemail = $app->get('mailfrom');

if ($task === 'edit' || $layout === 'form')
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}


// Add template js

JHtml::_('script', 'popper.min.js', array('version' => 'auto', 'relative' => true));
JHtml::_('script', 'bootstrap.min.js', array('version' => 'auto', 'relative' => true));
JHtml::_('script', 'ie10-viewport-bug-workaround.js', array('version' => 'auto', 'relative' => true));

// Add Stylesheets
JHtml::_('stylesheet', 'bootstrap.min.css', array('version' => 'auto', 'relative' => true));
JHtml::_('stylesheet', 'dizi.css', array('version' => 'auto', 'relative' => true));

// Adjusting content width
$position7ModuleCount = $this->countModules('position-7');
$position8ModuleCount = $this->countModules('position-8');
$position10ModuleCount = $this->countModules('position-10');

if ($position7ModuleCount && $position8ModuleCount)
{
	$span = 'span6';
}
elseif ($position7ModuleCount && !$position8ModuleCount)
{
	$span = 'span9';
}
elseif (!$position7ModuleCount && $position8ModuleCount)
{
	$span = 'span9';
}
else
{
	$span = 'span12';
}

// Logo file or site title param
if ($this->params->get('logoFile'))
{
	$logo = '<img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename . '" />';
}
elseif ($this->params->get('sitetitle'))
{
	$logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars($this->params->get('sitetitle'), ENT_COMPAT, 'UTF-8') . '</span>';
}
else
{
	$logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
}
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jdoc:include type="head" />
</head>
<body class="site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. ($params->get('fluidContainer') ? ' fluid' : '')
	. ($this->direction === 'rtl' ? ' rtl' : '');
?>">
<!-- Header -->
<nav class="navbar navbar-light bg-light">
	<div class="container">
		<a class="navbar-brand float-left" href="<?php echo $this->baseurl; ?>/">
		<?php echo $logo; ?>
		<?php if ($this->params->get('sitedescription')) : ?>
			<?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription'), ENT_COMPAT, 'UTF-8') . '</div>'; ?>
		<?php endif; ?>
			</a>
		<div class="float-right">
			<div class="hamburger-menu d-inline-block">
				<a href="#" data-toggle="modal" data-target="#navigation"> <span class="icon-hamburger"></span> </a>
			</div>
			<div class="lang d-inline-block"> <a href="">En</a> </div>
		</div>
	</div>
</nav>

<!-- Modal -->
<div class=" navigation modal fade" id="navigation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<a href="" class="close" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true"><span class="icon-close"></span></span>
	</a>
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-body text-center">
					<jdoc:include type="modules" name="position-7" style="well" />
			</div>
		</div>
	</div>
</div>
<?php if ($this->countModules('position-12')){ ?>
		<jdoc:include type="modules" name="position-12" style="xhtml" />
<?php } ?>

<?php if (!$this->countModules('position-12')){ ?>
<!-- Content -->
<div class="project">
	<div class="container">
				<!-- Begin Content -->
				<jdoc:include type="message" />
				<jdoc:include type="component" />
				<div class="clearfix"></div>
				<!-- End Content -->
	</div>
</div>
<?php } ?>

<!--Footer-->
<footer class="py-4">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-9 d-none d-md-block">
				<a href="mailto:<?= $siteemail; ?>" class="email d-block"><?= $siteemail; ?></a>
				<jdoc:include type="modules" name="position-10" style="xhtml" />
			</div>
			<div class="col-12 col-md-3 text-md-right text-center">
				&copy; <?php echo $sitename; ?> 2001 - <?php echo date('Y'); ?>
			</div>
		</div>
	</div>
</footer>

	<jdoc:include type="modules" name="debug" style="none" />
</body>
</html>
